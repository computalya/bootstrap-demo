# README  

## Description

DemoApp with

- Bootstrap 4.1.0
- Rails 5.1.6


#### create a new demoapp  

```bash
rails new bootstrap-demo -B -T
cd bootstrap-demo
```  

#### add new gems to [Gemfile]  

```bash
gem 'bootstrap'
```  

> run bundle command

`bundle`

#### create sample pages using rails controller generator

`rails g controller Pages home`  

#### configure root page [config/routes.rb]

`root 'pages#home'`  

#### rename application.css and update


`mv app/assets/stylesheets/application.css app/assets/stylesheets/application.scss` 

> application.scss

```scss
@import "Bootstrap";
```

#### install JQuery using yarn  

`yarn add jquery`  

#### update [app/assets/javascripts/application.js]

add jquery, popper(needed for bootstrap) and bootstrap

```js
//= require jquery
//= require popper
//= require bootstrap
//= require rails-ujs
//= require activestorage
//= require turbolinks
//= require_tree .
```  

> compile all assets

`rails assets:precompile`  


#### update: [app/views/pages/home.html.erb]

```html
<button type="button" class="btn btn-primary">Primary</button>
<button type="button" class="btn btn-secondary">Secondary</button>
<button type="button" class="btn btn-success">Success</button>
<button type="button" class="btn btn-danger">Danger</button>
<button type="button" class="btn btn-warning">Warning</button>
<button type="button" class="btn btn-info">Info</button>
<button type="button" class="btn btn-light">Light</button>
<button type="button" class="btn btn-dark">Dark</button>

<button type="button" class="btn btn-link">Link</button>


<div class="dropdown">
  <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
    Dropdown button
  </button>
  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
    <a class="dropdown-item" href="#">Action</a>
    <a class="dropdown-item" href="#">Another action</a>
    <a class="dropdown-item" href="#">Something else here</a>
  </div>
</div>
```
